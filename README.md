<b>Description:</b><br>
Ruby script which provides command line interface with Zeus.<br>

<b>Requirements:</b><br>
Ruby 2.3.0<br>
bundler<br>

<b>Installation:</b><br>
Install ruby 2.3.0 or greater.<br>
Install bundler (gem install bundler)<br>
Clone this repository.<br>

Once you have everything set up you can execute the script by changing to the repo's directory and running:<br>
  bundle install<br>
  ruby zeus_deploy.rb --env env_name --action_type action_type --release release_number<br>

<b>Script Options:</b><br>
The options are case-sensitive and must be lowercase.<br>

There are 3 required options:<br>
  '-a', '--action',   The action you want to execute<br>
  '-e', '--env',      The environment you want to perform an action on<br>
  '-r', '--release',  The release you want for deployment<br>

And 2 optional options:<br>
  '-f', '--config',        The path to the configuration file. File must be yaml format<br>
  '-s', '--component_set', Perform a bulk action on a pre-defined set of components<br>

<b>Script Actions:</b><br>
These are the current available actions:<br>
'create'      - creates a named environment. It must use one of the names defined in the stars_env_definitions.yaml file.<br>
'reconfigure' - reconfigures all components of an existing environment. This will get the latest artifact values from ADB during this action.<br>
'destroy'     - destroys an existing environment.<br>
'extend'      - extends the environment end date by 15 days from time of execution.<br>
'stop'        - stops all components of an existing environment.<br>
'reset'       - resets all components of an existing environment.<br>

<b>Environment Definitions</b><br>
Environment definitions are managed in the ./data/stars_env_defintions.yaml file. This file tells Zeus the name of each environment and defines its respective: blueprint, topology, parent component, chef_environment, and group.<br>

New environments must be defined in this file before they can be created!<br>

<b>Component Set Definitions:</b><br>
Component set definitions are managed in the ./data/component_set_defintions.yaml file. This file defines sets of components to perform actions on. This is helpful for when you want to create Jenkins jobs to restart the Stella related applications in a Zeus STARS environment. Some actions like destroy cannot be performed within a set. It will destroy the entire environment.<br>

<b>Configuration:</b><br>
The Zeus configuration file contains the information required to make HTTP requests to the Zeus API. The file must be in YAML format!<br>

The file can be read in from multiple sources. Once it finds a file it will not continue looking. The script will check for the file using the following precendence:

Command Line:<br>
File can be read in from the command line using the '-f' option mentioned above. Provide the full path to the file including the filename. It must be in YAML format.<br>

Ex: -f /opt/zeus_deploy/zeus_config.yaml<br>

Home directory:<br>
The script will automatically check in this location and filename of the home directory: ~/.zeus/zeus_config.yaml.<br>

Default location:<br>
If the script does not find a config file from the previous options it will check for the default one that comes with the script at: ./data/zeus_config.yaml<br>

<b>Examples:</b><br>
Create cell1 environment with master artifacts:<br>
ruby zeus_deploy.rb -e cell1 -a create -r starsmaster<br>

Reconfigure cell3 environment with 8.13 artifacts using command line Zeus configuration file:<br>
ruby zeus_deploy.rb --env cell3 --action reconfigure --release 8.13 --config /www/a/config/zeus_config.yaml<br>

Stop stella_mcom components in the pds-dev environment using a command line Zeus configuration file:<br>
ruby zeus_deploy.rb --env pds-dev -a stop -r starsmaster -f ~/.zeus/zeus_config.yaml -s stella_mcom<br>