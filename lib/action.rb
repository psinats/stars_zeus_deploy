# frozen_string_literal: true

# Action class handles creating the Zeus action
class Action
  attr_reader :action_type
  attr_reader :api
  attr_reader :env

  def initialize(opts)
    @action_type   = opts[:action]
    @release       = opts[:release]
    @config = \
      if opts[:config].nil?
        nil
      else
        opts[:config]
      end
    @component_set = \
      if opts[:component_set].nil?
        nil
      else
        opts[:component_set]
      end
    @db_set = \
      if opts[:db_set].nil?
        nil
      else
        opts[:db_set]
      end

    @api = ZeusApi.new(@release, @action_type, @config)
    @env = Environment.new(opts[:env], @api, @release, @component_set, @action_type, @db_set)
  end

  def process_action
    case @action_type
    when 'reconfigure'
      reconfigure
    when 'create'
      create
    when 'destroy'
      destroy
    when 'extend'
      extend_env
    when 'stop', 'configure', 'deploy'
      bulk_action
    else
      raise "Illegal action: '#{@action_type}' selected."
    end
  end

  def reconfigure
    env_id = @env.env_id

    if @api.environment_pending(env_id)
      raise "Environment already in state: '#{state}'. Action cannot be completed."
    end

    payload = {
      'follow_dependencies' => true,
      'parameters' => {
        @env.component => {
          'components' => @env.reconfigure_parameters
        }
      }
    }

    @api.reconfigure_environment(env_id, payload)

    extend_env
  end

  def create
    if @api.environment_exists(@env.env_name)
      raise 'Environment already exists. Action cannot be completed.'
    end

    create_payload = {
      'name'             => @env.env_name,
      'source_blueprint' => @env.blueprint_id,
      'source_topology'  => @env.topology_id,
      'group'            => @env.group,
      'metadata'         => @env.metadata,
      'params'           => @env.parameters
    }

    @api.create_environment(create_payload)
    env_id = @env.env_id

    lifecycle_payload = {
      'target_type'     => 'environment',
      'target_id'       => env_id,
      'target_action'   => 'start',
      'execute_once_at' => DateTime.now.new_offset(0)
    }

    @api.lifecycle(env_id, lifecycle_payload)

    extend_env
  end

  def destroy
    env_id = @env.env_id
    return if env_id.nil?

    @api.delete_environment(env_id)
  end

  def extend_env(hours: 360)
    env_id       = @env.env_id
    destroy_time = DateTime.now.new_offset(0) + (hours.to_i / 24.0)

    payload = {
      'target_type'       => 'environment',
      'target_id'         => env_id,
      'target_action'     => 'stop',
      'execute_once_at'   => destroy_time,
      'delete_duplicates' => true
    }

    @api.lifecycle(env_id, payload)
  end

  def bulk_action
    env_id = @env.env_id

    if @api.environment_pending(env_id)
      raise "Environment already in state: '#{state}'. Action cannot be completed."
    end

    payload = @env.bulk_action_parameters

    @api.bulk_action_environment(env_id, payload)
  end
end
