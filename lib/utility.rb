# frozen_string_literal: true

# Utility module provides general helper methods
module Utility
  def self.process_options
    valid_actions   = %w[create destroy reconfigure extend stop deploy configure]
    valid_comp_sets = YAML.load_file('./data/component_set_definitions.yaml').keys
    valid_db_sets   = YAML.load_file('./data/db_set_definitions.yaml').keys

    opts = Slop.parse do |o|
      o.string '-a', '--action',        "The action you want to execute. Available actions: #{valid_actions.join(', ')}", required: true
      o.string '-e', '--env',           'The environment you want to perform an action on', required: true
      o.string '-r', '--release',       'The release you want for deployment', required: true
      o.string '-f', '--config',        'The full path to the configuration file. File must be yaml format.', required: false
      o.string '-s', '--component_set', "Perform action on a set of components. Available sets: #{valid_comp_sets.join(', ')}", required: false
      o.string '-d', '--db_set',        "Perform reconfigure action with specified database set. Available sets: #{valid_db_sets.join(', ')}", required: false
    end

    unless valid_actions.include?(opts[:action])
      puts opts
      raise "Invalid action: '#{opts[:action]}' selected."
    end

    unless opts[:component_set].nil?
      unless valid_comp_sets.include?(opts[:component_set])
        puts opts
        raise "Invalid component set selected: #{opts[:component_set]}."
      end
    end

    unless opts[:db_set].nil?
      unless valid_db_sets.include?(opts[:db_set])
        puts opts
        raise "Invalid database set selected: #{opts[:db_set]}."
      end
    end

    opts.to_hash
  end
end
