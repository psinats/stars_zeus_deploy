# frozen_string_literal: true

# API class handles Zeus configuration to make API requests
class ZeusApi
  attr_reader :zeus_user
  attr_reader :zeus_email
  attr_reader :zeus_host

  API_V1_PATH          = '/api/v1/'
  API_BLUEPRINT_PATH   = '/blueprints/'
  API_TOPOLOGIES_PATH  = '/topologies/'
  API_ENVIRONMENT_PATH = '/environments/'
  API_LIFECYCLE_PATH   = '/lifecycle_events/'
  API_ADB              = '/services/adb/'

  def initialize(release, action_type, config)
    @release     = release
    @action_type = action_type
    @config      = config

    zeus_config  = process_zeus_config_file
    @zeus_host   = zeus_config['host']
    @zeus_token  = zeus_config['token']
    @zeus_user   = zeus_config['username']
    @zeus_email  = zeus_config['email']

    @api_path    = File.join(@zeus_host, API_V1_PATH)
    @max_checks  = 600
  end

  def process_zeus_config_file
    file_list = [
      '~/.zeus/zeus_config.yaml',
      './data/zeus_config.yaml'
    ]

    file_list.unshift(@config) unless @config.nil?

    file_list.each do |file|
      if File.exist?(file)
        puts "Configuration file loaded from: #{file}"
        return YAML.load_file(file)
      end
    end

    raise 'No configuration file found'
  end

  def request(type: nil, api_string: nil, payload: {})
    headers = {
      'Accept'        => 'application/json',
      'Authorization' => "Token #{@zeus_token}",
      'Content-Type'  => 'application/json'
    }

    response = \
      case type
      when 'get'
        RestClient.get(File.join(@api_path, api_string.to_s), headers)
      when 'delete'
        RestClient.delete(File.join(@api_path, api_string.to_s), headers)
      when 'post'
        RestClient.post(File.join(@api_path, api_string.to_s), payload.to_json, headers)
      else
        raise "Unhandled request type: '#{type}' attempted"
      end

    puts "#{Time.now.strftime('%H:%M:%S')} #{type.upcase} #{File.join(@api_path, api_string.to_s)} #{response.code}"

    JSON.parse(response.body)
  end

  def create_environment(payload)
    request(type: 'post',
            api_string: File.join(API_ENVIRONMENT_PATH),
            payload: payload)
  end

  def reconfigure_environment(env_id, payload)
    request(type: 'post',
            api_string: File.join(API_ENVIRONMENT_PATH, env_id, 'configure/'),
            payload: payload)

    wait_for_deployment(env_id)
  end

  def delete_environment(env_id)
    request(type: 'delete',
            api_string: File.join(API_ENVIRONMENT_PATH, "#{env_id}/"))

    wait_for_deployment(env_id)
  end

  def bulk_action_environment(env_id, payload)
    request(type: 'post',
            api_string: File.join(API_ENVIRONMENT_PATH, env_id, 'actions/'),
            payload: payload)

    wait_for_deployment(env_id)
  end

  def lifecycle(env_id, payload)
    request(type: 'post',
            api_string: File.join(API_LIFECYCLE_PATH),
            payload: payload)

    wait_for_deployment(env_id)
  end

  def wait_for_deployment(env_id)
    @max_checks.times do
      t     = Time.now
      state = get_environment_state(env_id)

      return state if %w[success destroyed].include?(state)

      if %w[failure destroy_failed].include?(state)
        raise "Action: '#{@action_type}' completed with state: '#{state}'"
      end

      sleep(t + 30 - Time.now)
    end

    raise "Timed out after #{@max_checks} tries"
  end

  def get_actions(env_id)
    request(type: 'get',
            api_string: File.join(API_ENVIRONMENT_PATH, env_id, 'actions/'))
  end

  def get_env_id(env_name)
    all_envs = request(type: 'get',
                       api_string: File.join(API_ENVIRONMENT_PATH))

    env = all_envs.find { |x| x['name'] == env_name && x['deleted'] == false }

    if env.nil?
      if @action_type == 'destroy'
        puts "Environment: '#{env_name}' not found. Nothing to destroy"
        return nil
      end

      raise "Environment: '#{env_name}' not found"
    end

    env['id']
  end

  def get_environment_state(env_id)
    response = request(type: 'get',
                       api_string: File.join(API_ENVIRONMENT_PATH, "#{env_id}/"))

    response['state'].to_s
  end

  def environment_pending(env_id)
    state = get_environment_state(env_id)

    %w[pending destroying].include?(state)
  end

  def environment_exists(env_name)
    all_envs = request(type: 'get',
                       api_string: File.join(API_ENVIRONMENT_PATH))

    env = all_envs.find { |x| x['name'] == env_name && x['deleted'] == false }
    !env.nil?
  end

  def get_adb_info(location)
    location.slice!(API_V1_PATH)

    request(type: 'get',
            api_string: "#{location}&release=#{@release}")
  end

  def get_blueprint_id(blueprint_name)
    all_blueprints = request(type: 'get',
                             api_string: API_BLUEPRINT_PATH)

    env_hash = all_blueprints.find { |h| h['name'] == blueprint_name }
    t = env_hash['versions']
    blueprint_version = t.max_by { |h| h['version'].to_i }['version'].to_i

    if (r = t.find { |h| h['version'].to_i == blueprint_version })
      return r['id']
    end

    raise "Could not find blueprint: '#{@env.blueprint}'"
  end

  def get_blueprint_file(blueprint_name)
    blueprint_id = get_blueprint_id(blueprint_name)

    request(type: 'get',
            api_string: File.join(API_BLUEPRINT_PATH, blueprint_id))
  end

  def get_topology_id(topology_name)
    all_topologies = request(type: 'get',
                             api_string: API_TOPOLOGIES_PATH)

    env_hash = all_topologies.select { |h| h['name'] == topology_name }
    topology_version = env_hash.max_by { |h| h['version'].to_i }['version'].to_i

    if (r = env_hash.find { |h| h['version'].to_i == topology_version })
      return r['id']
    end

    raise "Could not find topology: '#{topology_name}'"
  end
end
