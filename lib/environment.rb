# frozen_string_literal: true

# class Environment
class Environment
  attr_reader :env_name
  attr_reader :blueprint
  attr_reader :topology
  attr_reader :component
  attr_reader :chef_environment
  attr_reader :group
  attr_reader :blueprint_id
  attr_reader :topology_id

  def initialize(env_name, api, release, component_set, action_type, db_set)
    @env_name         = env_name
    @release          = release
    @component_set    = component_set
    @action_type      = action_type
    @db_set           = db_set

    env_definition    = YAML.load_file('./data/stars_env_definitions.yaml')

    if env_definition[@env_name].nil?
      raise "Environment '#{env_name}' not defined in stars_env_definitions.yaml file"
    end

    @component        = env_definition[@env_name]['component']
    @blueprint        = env_definition[@env_name]['blueprint']
    @topology         = env_definition[@env_name]['topology']
    @chef_environment = env_definition[@env_name]['chef_environment'] ||= '_default'
    @group            = env_definition[@env_name]['group'] ||= 'default'

    @api              = api
    @blueprint_id     = @api.get_blueprint_id(@blueprint)
    @topology_id      = @api.get_topology_id(@topology)
  end

  def env_id
    @api.get_env_id(@env_name)
  end

  def process_parameters(param)
    if param['type'] == 'macys_artifact'
      artifact_values = @api.get_adb_info(param['choices']['location'])

      {
        'release'  => @release,
        'version'  => artifact_values['data'].values[0]['version'],
        'location' => artifact_values['data'].values[0]['location']
      }
    elsif param['name'] == 'chef_environment'
      @chef_environment
    elsif param['name'] == 'release'
      @release
    else
      param['default']
    end
  end

  def parameters
    param_hash     = {}
    blueprint_hash = @api.get_blueprint_file(@blueprint)

    blueprint_hash['component']['parameters'].each do |parent_param|
      param_key = "#{blueprint_hash['component']['name']}.#{parent_param['name']}"
      param_hash[param_key] = process_parameters(parent_param)
    end

    blueprint_hash['component']['components'].each do |child_component|
      child_component['parameters'].each do |child_param|
        param_key = "#{blueprint_hash['component']['name']}.#{child_component['name']}.#{child_param['name']}"
        param_hash[param_key] = process_parameters(child_param)
      end
    end

    unless @db_set.nil?
      db_param_hash = db_set_definitions(blueprint_hash['component']['name'], param_hash)
      return db_param_hash
    end

    param_hash
  end

  def reconfigure_parameters
    param_hash     = {}
    blueprint_hash = @api.get_blueprint_file(@blueprint)
    component_list = component_list(blueprint_hash)

    component_list.each do |child_component|
      param_hash[child_component] = { 'parameters' => {} }
      child_hash = blueprint_hash['component']['components'].find { |c| c['name'] == child_component }
      next if child_hash.nil?

      child_hash['parameters'].each do |param|
        param_hash[child_component]['parameters'][param['name']] = process_parameters(param)
      end
    end

    param_hash
  end

  def bulk_action_parameters
    payload        = []
    blueprint_hash = @api.get_blueprint_file(@blueprint)
    comp_list      = component_list(blueprint_hash)

    comp_list.each do |component|
      payload << {
        'type'      => 'environment',
        'component' => component,
        'action'    => @action_type
      }
    end

    payload
  end

  def component_list(blueprint_hash)
    comp_list     = []
    invalid_list  = %w[f5 stella_reindex webdav tibco activemq nfs preview zookeeper]

    if @action_type == 'reconfigure'
      if @component_set.nil?
        blueprint_hash['component']['components'].each { |c| comp_list << c['name'] }
        comp_list -= invalid_list
      else
        comp_list = component_set_definitions
      end
    else
      filtered_list = []
      action_list   = @api.get_actions(env_id)

      action_list.each do |comp, action_set|
        formatted_key = comp.split('.').last.to_s
        filtered_list << formatted_key if action_set.include?(@action_type)
      end

      comp_list = \
        if @component_set.nil?
          filtered_list - invalid_list
        else
          filtered_list & component_set_definitions
        end

      comp_list.map { |comp| comp.to_s.prepend("#{@component}.") }
    end

    comp_list
  end

  def component_set_definitions
    comp_set_def = YAML.load_file('./data/component_set_definitions.yaml')
    comp_set_def.dig(@component_set, 'components')
  end

  def db_set_definitions(parent_component, param_hash)
    db_set_def = YAML.load_file('./data/db_set_definitions.yaml')
    new_db_set = db_set_def[@db_set]

    new_db_set.each do |k, _v|
      param_hash["#{parent_component}.db.#{k}"]['host'] = new_db_set[k]
    end

    param_hash
  end

  def metadata
    {
      'fqdn' => {
        'value' => "#{@env_name}.tbe.zeus.fds.com",
        'type'  => 'zeus.core.string'
      },
      'dnsZone' => {
        'value' => 'tbe.zeus.fds.com',
        'type'  => 'zeus.core.string'
      },
      'owner' => {
        'type'  => 'zeus.core.string',
        'value' => @api.zeus_user
      },
      'contactEmails' => {
        'value' => [
          @api.zeus_email
        ],
        'type' => 'zeus.core.list'
      }
    }
  end
end
