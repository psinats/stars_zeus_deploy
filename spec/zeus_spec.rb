# frozen_string_literal: true

describe 'zeus' do
  before do
    allow($stdout).to receive(:write)

    opts = {
      env: 'stars-test',
      action: 'reconfigure',
      release: 'starsmaster'
    }

    @zeus_action = Action.new(opts)
  end

  context 'Check if zeus_config.yaml is in proper YAML format' do
    it 'Read in zeus configuration file' do
      expect(@zeus_action.api.zeus_host).to eql('https://mdc2vrb056.zeus.fds.com/')
      expect(@zeus_action.api.zeus_user).to eql('jenkins')
      expect(@zeus_action.api.zeus_email).to eql('no_reply@macys.com')
    end
  end

  context 'Check if stars_env_definitions.yaml is in proper YAML format' do
    it 'Read in stars_env_definitions file' do
      expect(@zeus_action.env.blueprint).to eql('stars_test')
      expect(@zeus_action.env.topology).to eql('stars_test')
      expect(@zeus_action.env.component).to eql('stars_test')
      expect(@zeus_action.env.chef_environment).to eql('_default')
      expect(@zeus_action.env.group).to eql('default')
    end
  end

  context 'Check if component_set_definitions.yaml is in proper YAML format' do
    it 'Read in component_set_definitions file' do
      comp_set_def = YAML.load_file('./data/component_set_definitions.yaml')
      test_set     = comp_set_def.dig('stella_mcom', 'components')

      expect(test_set).to eql(%w[stella_mcom_async1 stella_mcom_async2 stella_mcom_indexer stella_mcom_searcher stella_mcom_ui])
    end
  end

  context 'Test ability to complete successful request to Zeus API' do
    it 'get test env blueprint id' do
      expect(@zeus_action.api.get_blueprint_id(@zeus_action.env.blueprint)).to eql('13957dec-d717-4473-86ec-1047fea1b368')
    end
  end
end
