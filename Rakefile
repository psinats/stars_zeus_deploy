# frozen_string_literal: true

require 'rubygems'
require 'bundler'

Bundler.require

require 'json'
require 'yaml'
require 'date'

require_relative 'lib/utility.rb'
require_relative 'lib/action.rb'
require_relative 'lib/environment.rb'
require_relative 'lib/zeus_api.rb'

task default: ['test']

desc 'test by creating, reconfiguring, and destroying a test environment'
task :test do
  Rake::Task[:create].invoke
  Rake::Task[:reconfigure].invoke
  Rake::Task[:destroy].invoke
end

desc 'create Zeus environment'
task :create, %i[env release] do |_t, args|
  args.with_defaults(env: 'stars-test', release: 'starsmaster')
  opts = {
    env: args.env,
    action: 'create',
    release: args.release
  }

  Action.new(opts).process_action
end

desc 'reconfigure Zeus environment'
task :reconfigure, %i[env release] do |_t, args|
  args.with_defaults(env: 'stars-test', release: 'starsmaster')
  opts = {
    env: args.env,
    action: 'reconfigure',
    release: args.release
  }

  Action.new(opts).process_action
end

desc 'destroy Zeus environment'
task :destroy, %i[env release] do |_t, args|
  args.with_defaults(env: 'stars-test', release: 'starsmaster')
  opts = {
    env: args.env,
    action: 'destroy',
    release: args.release
  }

  Action.new(opts).process_action
end

desc 'extend Zeus environment'
task :extend, %i[env release] do |_t, args|
  args.with_defaults(env: 'stars-test', release: 'starsmaster')
  opts = {
    env: args.env,
    action: 'extend',
    release: args.release
  }

  Action.new(opts).process_action
end

desc 'stop Zeus environment'
task :stop, %i[env release] do |_t, args|
  args.with_defaults(env: 'stars-test', release: 'starsmaster')
  opts = {
    env: args.env,
    action: 'stop',
    release: args.release
  }

  Action.new(opts).process_action
end
