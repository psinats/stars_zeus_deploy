# frozen_string_literal: true

require 'rubygems'
require 'bundler'

Bundler.require

require 'json'
require 'yaml'
require 'date'

require_relative 'lib/utility.rb'
require_relative 'lib/action.rb'
require_relative 'lib/environment.rb'
require_relative 'lib/zeus_api.rb'

# Process script options
opts = Utility.process_options
puts "You have selected to '#{opts[:action]}' the '#{opts[:env]}' environment."

Action.new(opts).process_action
